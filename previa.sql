-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: infinityspaces
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a_infinity`
--

DROP TABLE IF EXISTS `a_infinity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_infinity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `pessoas_chave_foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pessoas_chave_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `a_infinity`
--

LOCK TABLES `a_infinity` WRITE;
/*!40000 ALTER TABLE `a_infinity` DISABLE KEYS */;
INSERT INTO `a_infinity` VALUES (1,'pexels-photo_20161101153356.jpg','pexels-photo-69040_20161101153357.png','pexels-photo-90911_20161101153357.jpeg','pexels-photo-90911_20161101153357.jpeg','pexels-photo-128639_20161101153357.jpeg','abstract-art-depths-of-emotion_20161101153357.jpg','EXCELÊNCIA EM PROJETOS E EXECUÇÃO DE OBRAS','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nunc.</p>\r\n\r\n<h2>Quisque eget risus sit amet orci venenatis varius non quis arcu.</h2>\r\n\r\n<p>Quisque et mi semper, tincidunt neque ut, pellentesque urna. Maecenas congue pretium egestas. Fusce id malesuada libero, faucibus sodales diam. Ut a diam quis est pretium facilisis. Duis quis nisi tristique, fringilla justo ac, aliquet turpis. Phasellus sed accumsan est, ac finibus ante. Nullam et sapien nulla.</p>\r\n','perfil-giseli_20161101153357.jpg','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nunc.</p>\r\n\r\n<p>Quisque eget risus sit amet orci venenatis varius non quis arcu. Quisque et mi semper, tincidunt neque ut, pellentesque urna. Maecenas congue pretium egestas.</p>\r\n\r\n<p>Fusce id malesuada libero, faucibus sodales diam. Ut a diam quis est pretium facilisis. Duis quis nisi tristique, fringilla justo ac, aliquet turpis. Phasellus sed accumsan est, ac finibus ante. Nullam et sapien nulla.</p>\r\n',NULL,'2016-11-01 15:33:57');
/*!40000 ALTER TABLE `a_infinity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'pexels-photo-69040_20161101152934.png','PROJETOS RESIDENCIAIS E<br />\r\nCORPORATIVOS DE ALTO PADR&Atilde;O','#','2016-11-01 15:29:35','2016-11-01 15:29:35'),(2,2,'pexels-photo-128639_20161101152951.jpeg','LOREM IPSUM DOLOR SIT<br />\r\nAMET CONSECTETUR','#','2016-11-01 15:29:52','2016-11-01 15:29:52');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','+55 11 3736-8600','Av. Marcos Penteado de Ulhoa Rodrigues, 1119<br />\r\nTambor&eacute; &middot;&nbsp;Barueri - SP<br />\r\n06460-040','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.730980975063!2d-46.83719114992086!3d-23.506197384635215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf01f669c0e13f%3A0xf364d27ca59b406d!2sAv.+Marcos+Penteado+de+Ulhoa+Rodrigues%2C+1119+-+Tambor%C3%A9%2C+Barueri+-+SP%2C+06460-040!5e0!3m2!1spt-BR!2sbr!4v1478015062151\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','#','#',NULL,'2016-11-01 15:45:38');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chamada_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicos_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `servicos_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` VALUES (1,'INOVANDO, ANTECIPANDO TENDÊNCIAS E ENTENDENDO COMPLETAMENTE O DESEJO DE CADA CLIENTE','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nunc. Quisque eget risus sit amet orci venenatis varius non quis arcu. Quisque et mi semper, tincidunt neque ut, pellentesque urna. Maecenas congue pretium egestas. Fusce id malesuada libero, faucibus sodales diam. Ut a diam quis est pretium facilisis. Duis quis nisi tristique, fringilla justo ac, aliquet turpis. Phasellus sed accumsan est, ac finibus ante. Nullam et sapien nulla.','sky-clouds-clouds-form-cumulus-clouds_20161101153221.jpg','pexels-photo-90911_20161101153222.jpeg','pexels-photo_20161101153222.jpg','NOSSO SERVIÇOS: FOCO EM SOLUÇÕES ADEQUADAS','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nunc. Quisque eget risus sit amet orci venenatis varius non quis arcu. Quisque et mi semper, tincidunt neque ut, pellentesque urna. Maecenas congue pretium egestas. Fusce id malesuada libero, faucibus sodales diam. Ut a diam quis est pretium facilisis. Duis quis nisi tristique, fringilla justo ac, aliquet turpis. Phasellus sed accumsan est, ac finibus ante. Nullam et sapien nulla.','EXCELÊNCIA EM PROJETOS E EXECUÇÃO DE OBRAS','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nunc.','img-ilustra-home_20161101153222.jpg',NULL,'2016-11-01 15:32:22');
/*!40000 ALTER TABLE `home` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia`
--

DROP TABLE IF EXISTS `midia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia`
--

LOCK TABLES `midia` WRITE;
/*!40000 ALTER TABLE `midia` DISABLE KEYS */;
INSERT INTO `midia` VALUES (1,0,'2016-11','Exemplo','exemplo','abstract-art-depths-of-emotion_20161101154239.jpg','2016-11-01 15:42:39','2016-11-01 15:42:39'),(2,0,'2016-04','Outro Exemplo','outro-exemplo','pexels-photo-90911_20161101154300.jpeg','2016-11-01 15:43:01','2016-11-01 15:43:01');
/*!40000 ALTER TABLE `midia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_imagens`
--

DROP TABLE IF EXISTS `midia_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `midia_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `midia_imagens_midia_id_foreign` (`midia_id`),
  CONSTRAINT `midia_imagens_midia_id_foreign` FOREIGN KEY (`midia_id`) REFERENCES `midia` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_imagens`
--

LOCK TABLES `midia_imagens` WRITE;
/*!40000 ALTER TABLE `midia_imagens` DISABLE KEYS */;
INSERT INTO `midia_imagens` VALUES (1,1,0,'abstract-art-depths-of-emotion_20161101154244.jpg','2016-11-01 15:42:44','2016-11-01 15:42:44'),(2,1,0,'pexels-photo_20161101154244.jpg','2016-11-01 15:42:44','2016-11-01 15:42:44'),(3,2,0,'sky-clouds-clouds-form-cumulus-clouds_20161101154315.jpg','2016-11-01 15:43:16','2016-11-01 15:43:16');
/*!40000 ALTER TABLE `midia_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_10_21_180158_create_newsletter_table',1),('2016_10_21_183855_create_servicos_table',1),('2016_10_21_185443_create_servicos_abertura_table',1),('2016_10_25_004220_create_banners_table',1),('2016_10_25_004425_create_home_table',1),('2016_10_25_004629_create_a_infinity_table',1),('2016_10_25_004712_create_midia_table',1),('2016_10_25_141750_create_projetos_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projetos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conceito` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desafios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`),
  CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,1,1,'pexels-photo-69040_20161101153953.png','Exemplo','exemplo','São Paulo - SP','1.200m²','2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nun','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nun','2016-11-01 15:39:53','2016-11-01 15:39:53'),(2,1,2,'pexels-photo-128639_20161101154124.jpeg','Outro Exemplo','outro-exemplo','São Paulo - SP','1.200m²','2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nun','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nun','2016-11-01 15:41:24','2016-11-01 15:41:24');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_categorias`
--

DROP TABLE IF EXISTS `projetos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_categorias`
--

LOCK TABLES `projetos_categorias` WRITE;
/*!40000 ALTER TABLE `projetos_categorias` DISABLE KEYS */;
INSERT INTO `projetos_categorias` VALUES (1,1,'residencial','residencial','2016-11-01 15:39:03','2016-11-01 15:39:03'),(2,2,'comercial','comercial','2016-11-01 15:39:06','2016-11-01 15:39:06'),(3,3,'corporativo','corporativo','2016-11-01 15:39:09','2016-11-01 15:39:09');
/*!40000 ALTER TABLE `projetos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (1,1,0,'abstract-art-depths-of-emotion_20161101154036.jpg','2016-11-01 15:40:37','2016-11-01 15:40:37'),(2,1,0,'pexels-photo-69040_20161101154036.png','2016-11-01 15:40:38','2016-11-01 15:40:38'),(4,1,0,'pexels-photo-128639_20161101154043.jpeg','2016-11-01 15:40:44','2016-11-01 15:40:44');
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens_destaque`
--

DROP TABLE IF EXISTS `projetos_imagens_destaque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens_destaque` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_destaque_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_destaque_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens_destaque`
--

LOCK TABLES `projetos_imagens_destaque` WRITE;
/*!40000 ALTER TABLE `projetos_imagens_destaque` DISABLE KEYS */;
INSERT INTO `projetos_imagens_destaque` VALUES (1,1,0,'sky-clouds-clouds-form-cumulus-clouds_20161101154007.jpg','2016-11-01 15:40:08','2016-11-01 15:40:08'),(2,1,0,'pexels-photo-69040_20161101154007.png','2016-11-01 15:40:09','2016-11-01 15:40:09'),(3,1,0,'pexels-photo-90911_20161101154015.jpeg','2016-11-01 15:40:16','2016-11-01 15:40:16'),(4,1,0,'pexels-photo-128639_20161101154015.jpeg','2016-11-01 15:40:16','2016-11-01 15:40:16'),(5,2,0,'sky-clouds-clouds-form-cumulus-clouds_20161101154147.jpg','2016-11-01 15:41:49','2016-11-01 15:41:49'),(6,2,0,'pexels-photo-128639_20161101154147.jpeg','2016-11-01 15:41:49','2016-11-01 15:41:49');
/*!40000 ALTER TABLE `projetos_imagens_destaque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,1,'ESTUDO DE VIABILIDADE','ico-estudosviabilidade_20161101153540.png','<p>Lorem ipsum</p>\r\n\r\n<p>Dolor sit amet</p>\r\n\r\n<p>Consectetur</p>\r\n','2016-11-01 15:35:40','2016-11-01 15:35:40'),(2,2,'PROJETOS','ico-projetos_20161101153557.png','<p>Lorem ipsum</p>\r\n\r\n<p>Dolor sit amet</p>\r\n\r\n<p>Consectetur</p>\r\n','2016-11-01 15:35:57','2016-11-01 15:35:57'),(3,3,'GERENCIAMENTO DE PROJETOS','ico-credenciamento_20161101153631.png','<p>Lorem ipsum</p>\r\n\r\n<p>Dolor sit amet</p>\r\n\r\n<p>Consectetur</p>\r\n','2016-11-01 15:36:31','2016-11-01 15:36:31'),(4,4,'TURNKEY','ico-turnkey_20161101153651.png','<p>Lorem ipsum</p>\r\n\r\n<p>Dolor sit amet</p>\r\n\r\n<p>Consectetur</p>\r\n','2016-11-01 15:36:51','2016-11-01 15:36:51'),(5,5,'EXECUÇÃO DE OBRA','ico-execucaoobra_20161101153701.png','<p>Lorem ipsum</p>\r\n\r\n<p>Dolor sit amet</p>\r\n\r\n<p>Consectetur</p>\r\n','2016-11-01 15:37:01','2016-11-01 15:37:01'),(6,6,'PRODUÇÃO','ico-producao_20161101153731.png','<p>Lorem ipsum</p>\r\n','2016-11-01 15:37:31','2016-11-01 15:37:31');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos_abertura`
--

DROP TABLE IF EXISTS `servicos_abertura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos_abertura` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos_abertura`
--

LOCK TABLES `servicos_abertura` WRITE;
/*!40000 ALTER TABLE `servicos_abertura` DISABLE KEYS */;
INSERT INTO `servicos_abertura` VALUES (1,'AGILIDADE E PRECISÃO = NOSSOS SERVIÇOS','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo nisl, vehicula quis faucibus sed, venenatis ut risus. Nunc non elementum purus. Fusce id pharetra eros, at auctor ipsum. Quisque augue ligula, pulvinar eget volutpat ut, varius sed nunc. Quisque eget risus sit amet orci venenatis varius non quis arcu. Quisque et mi semper, tincidunt neque ut, pellentesque urna. Maecenas congue pretium egestas.</p>\r\n','pexels-photo-69040_20161101153447.png','pexels-photo-128639_20161101153447.jpeg',NULL,'2016-11-01 15:34:48');
/*!40000 ALTER TABLE `servicos_abertura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$LuP5EL2T6sTfPBhkeZXjp.GEE6ZqrgZfRtGKVpNAlvqsWi6SoCJ26','imhUzb2Fgu5ZFfiG5IDk1J7pRH5yfxftoXn0tanDCdE1oWfKxcfR6973a4QM',NULL,'2016-11-01 15:28:55');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-01 15:46:10
