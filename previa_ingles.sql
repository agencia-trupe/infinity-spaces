-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: infinityspaces
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a_infinity`
--

DROP TABLE IF EXISTS `a_infinity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_infinity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `pessoas_chave_foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pessoas_chave_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `pessoas_chave_texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `a_infinity`
--

LOCK TABLES `a_infinity` WRITE;
/*!40000 ALTER TABLE `a_infinity` DISABLE KEYS */;
INSERT INTO `a_infinity` VALUES (1,'img-0695_20161123145644.jpg','img-9340_20161123145644.jpg','img-0643_20161123145648.jpg','trump-towers-sunny-isles-beach-13_20161123145651.jpg','img-9622_20161123145654.jpg','img-2509b_20161123145654.jpg','EXCELÊNCIA EM PROJETOS E EXECUÇÃO DE OBRAS','','<p>A empresa desenvolve projetos que aliam sustentabilidade e tecnologia, comprometendo-se com a fun&ccedil;&atilde;o est&eacute;tica e a funcionalidade dos espa&ccedil;os, pesquisando materiais novos e adequados, sempre acompanhando as tend&ecirc;ncias do mercado, priorizando o conforto e o bem estar.&nbsp;</p>\r\n','','perfil-giseli_20161101153357.jpg','<p>Giseli Koraicho, designer de interiores formada pela Panamericana de Arte e Design, &agrave; frente da empresa Infinity Spaces, que atua na &aacute;rea de projetos de arquitetura e design de interiores, tem como principal meta tornar sonhos em realidade.</p>\r\n\r\n<p>Atuante no mercado residencial e corporativo, Giseli, tem como principal objetivo manter-se sempre atualizada e em constante aprimoramento profissional. S&oacute; assim, acredita ela, conseguimos bem atender e superar expectativas. Cada projeto &eacute; encarado como &uacute;nico e &eacute; sempre um novo desafio - &ldquo;Nada melhor ap&oacute;s a conclus&atilde;o de um projeto, sentir e comprovar que meu cliente sente-se acolhido no ambiente projetado e, mais do que isso, que sua personalidade est&aacute; marcada dentro de cada um dos espa&ccedil;os&rdquo;, diz a profissional que vem atuando no mercado h&aacute; mais de cinco anos.</p>\r\n\r\n<p>No mercado corporativo, Giseli, desenvolveu projetos para grandes empresas do mercado de escrit&oacute;rios, como a Infinity Officing Network, que demanda n&atilde;o apenas ambientes personalizados, mas infraestrutura e total capacidade de adapta&ccedil;&atilde;o de espa&ccedil;os de acordo com cada cliente.</p>\r\n\r\n<p>&ldquo;Escrit&oacute;rios s&atilde;o extens&otilde;es de nossas resid&ecirc;ncias, por isso o bem estar e a funcionalidade nesses locais devem ser tratados como prioridade na execu&ccedil;&atilde;o de um projeto. Hoje, passamos a maior parte de nosso tempo trabalhando e, por isso, sentir-se bem no seu ambiente de trabalho &eacute; essencial.&rdquo;, acredita Giseli, que mant&eacute;m seu foco nas necessidades individuais de cada empresa e na personaliza&ccedil;&atilde;o de cada projeto corporativo.&nbsp;</p>\r\n','',NULL,'2016-12-02 20:34:53');
/*!40000 ALTER TABLE `a_infinity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'img-0496_20161103201744.jpg','PROJETOS RESIDENCIAIS&nbsp;','','#','2016-11-01 15:29:35','2016-11-23 16:40:56'),(2,2,'img-2027_20161103202731.jpg','PROJETOS PERSONALIZADOS&nbsp;','','#','2016-11-01 15:29:52','2016-11-23 16:41:15'),(3,0,'img-2491b_20161123144142.jpg','PROJETOS CORPORTIVOS','','#','2016-11-23 16:41:48','2016-11-23 16:41:48'),(4,0,'trump-towers-sunny-isles-beach-12_20161124194810.jpg','ADMINISTRA&Ccedil;&Atilde;O<br />\r\nE GERENCIAMENTO DE PROJETOS','','#','2016-11-23 16:43:52','2016-11-24 21:48:15');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@infinityspaces.com','+55 11 3736-8600','ALAMEDA RIO NEGRO, 503<br />\r\n23&ordm; ANADAR<br />\r\nALPHAVILLE&nbsp;<br />\r\nBARUERI - SP<br />\r\nCEP: 06454-000','','https://www.google.com/maps/place/Alameda+Rio+Negro,+503+-+Alphaville+Industrial,+Barueri+-+SP,+Brasil/@-23.501213,-46.8482,16z/data=!4m5!3m4!1s0x94cf0223bfba0185:0x3b7e57ec00e3ed89!8m2!3d-23.5012132!4d-46.8481999?hl=pt-BR','#','#','2016-12-06 12:15:55','2016-12-06 12:16:24');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home`
--

DROP TABLE IF EXISTS `home`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chamada_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicos_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `servicos_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `servicos_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `servicos_texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `projetos_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home`
--

LOCK TABLES `home` WRITE;
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` VALUES (1,'INOVANDO, ANTECIPANDO TENDÊNCIAS E ENTENDENDO COMPLETAMENTE O DESEJO DE CADA CLIENTE','','A Infinity Spaces traz em seus mais de 20 anos de atua&ccedil;&atilde;o em projetos residenciais e corporativos, projetos de Arquitetura e Interiores inovadores, que antecipam tend&ecirc;ncias e entendem verdadeiramente as necessidades e expectativas de seus clientes.<br />\r\nCom escrit&oacute;rios em S&atilde;o Paulo e Miami (EUA), contamos com equipe de profissionais especializados, que desenvolvem projetos personalizados e diferenciados que atendem com exatid&atilde;o as expectativas de nossos clientes.','','img-2479_20161123144618.jpg','img-0875_20161123144627.jpg','img-2236_20161123144627.jpg','NOSSO SERVIÇOS: FOCO EM SOLUÇÕES ADEQUADAS','','Os projetos da Infinity Spaces buscam sempre o equil&iacute;brio dos espa&ccedil;os com sofistica&ccedil;&atilde;o, aliando solu&ccedil;&otilde;es t&eacute;cnicas e financeiras de acordo com cada&nbsp;perfil.<br />\r\nDesenvolvemos ambientes que aliam sustentabilidade e tecnologia, sempre pensando na import&acirc;ncia est&eacute;tica de cada elemento e a funcionalidade dos espa&ccedil;os.','','EXCELÊNCIA EM PROJETOS E EXECUÇÃO DE OBRAS','','Da concep&ccedil;&atilde;o ao espa&ccedil;o pronto - Desenvolvemos todas as etapas de seu projeto.','','img-2157_20161123131059.jpg',NULL,'2016-12-02 20:28:12');
/*!40000 ALTER TABLE `home` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia`
--

DROP TABLE IF EXISTS `midia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia`
--

LOCK TABLES `midia` WRITE;
/*!40000 ALTER TABLE `midia` DISABLE KEYS */;
INSERT INTO `midia` VALUES (1,0,'2012-11','Viverbem','','viverbem','capa_20161104121221.jpg','2016-11-01 15:42:39','2016-11-23 15:45:19'),(3,0,'2013-04','Revista Quartos & Closets','','revista-quartos-closets','capa2_20161104122304.jpg','2016-11-04 14:23:04','2016-11-23 15:46:09'),(4,0,'2012-11','Vero','','vero','vero_20161104122721.jpg','2016-11-04 14:27:21','2016-11-04 14:27:29'),(5,0,'2012-09','UOL','','uol','uol_20161104122820.jpg','2016-11-04 14:28:20','2016-11-04 14:28:20'),(6,0,'2012-07','Minimalismo','','minimalismo','minima_20161104123034.jpg','2016-11-04 14:30:35','2016-11-04 14:30:35'),(7,0,'2012-06','Revista Kaza','','revista-kaza','kaza1_20161104123212.jpg','2016-11-04 14:32:12','2016-11-04 14:32:12'),(8,0,'2012-05','Jornal Agora','','jornal-agora','agora_20161104123328.jpg','2016-11-04 14:33:29','2016-11-04 14:33:29'),(9,0,'2012-03','Gazeta de Piracicaba','','gazeta-de-piracicaba','gazeta_20161104123456.jpg','2016-11-04 14:34:56','2016-11-04 14:34:56'),(10,0,'2012-02','Decorar Mais por Menos','','decorar-mais-por-menos','decorar_20161104123707.jpg','2016-11-04 14:37:08','2016-11-04 14:37:08'),(11,0,'2012-01','Decoração e Estilo','','decoracao-e-estilo','dec1_20161104123939.jpg','2016-11-04 14:39:40','2016-11-04 14:39:40'),(12,0,'2012-01','DCasa','','dcasa','d1_20161104124830.jpg','2016-11-04 14:48:31','2016-11-04 14:48:31'),(13,0,'2011-12','Contemporânea','','contemporanea','charme1_20161104125740.jpg','2016-11-04 14:49:49','2016-11-04 14:57:40'),(14,0,'2011-11','Revista Construir','','revista-construir','co1_20161104125159.jpg','2016-11-04 14:51:59','2016-11-04 14:51:59'),(15,0,'2011-09','Casamix','','casamix','x1_20161104125507.jpg','2016-11-04 14:55:07','2016-11-04 14:55:07'),(16,0,'2011-08','Casapontocom','','casapontocom','ponto_20161104125615.jpg','2016-11-04 14:56:16','2016-11-04 14:56:16'),(17,0,'2011-08','Revista Casa & Decoração','','revista-casa-decoracao','casa_20161104125859.jpg','2016-11-04 14:58:59','2016-11-04 14:58:59'),(18,0,'2011-07','Anuário Vero','','anuario-vero','an1_20161104130004.jpg','2016-11-04 15:00:04','2016-11-04 15:00:04'),(19,0,'2010-06','Anuário Vero','','anuario-vero-1','anv1_20161104130109.jpg','2016-11-04 15:01:09','2016-11-04 15:01:09'),(20,0,'2010-03','A Magazine','','a-magazine','a1_20161104130203.jpg','2016-11-04 15:02:04','2016-11-04 15:02:04'),(21,0,'2016-06','Florida Design - Home & Decor','','florida-design-home-decor','miami-home-decor-cover_20161123132040.jpg','2016-11-23 15:20:41','2016-11-23 15:46:51'),(22,0,'2016-01','Casa Vogue','','casa-vogue','casa-vogue-capa_20161123133329.jpg','2016-11-23 15:33:29','2016-11-23 15:47:02'),(23,0,'2015-11','Anuário Stock & Home','','anuario-stock-home','anuario-stock-capa_20161123134140.jpg','2016-11-23 15:41:40','2016-11-23 15:47:16');
/*!40000 ALTER TABLE `midia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_imagens`
--

DROP TABLE IF EXISTS `midia_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `midia_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `midia_imagens_midia_id_foreign` (`midia_id`),
  CONSTRAINT `midia_imagens_midia_id_foreign` FOREIGN KEY (`midia_id`) REFERENCES `midia` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_imagens`
--

LOCK TABLES `midia_imagens` WRITE;
/*!40000 ALTER TABLE `midia_imagens` DISABLE KEYS */;
INSERT INTO `midia_imagens` VALUES (4,1,0,'3_20161104121242.jpg','2016-11-04 14:12:43','2016-11-04 14:12:43'),(5,1,0,'2_20161104121242.jpg','2016-11-04 14:12:43','2016-11-04 14:12:43'),(6,3,1,'22_20161104122433.jpg','2016-11-04 14:24:34','2016-11-04 14:24:34'),(7,3,2,'223_20161104122453.jpg','2016-11-04 14:24:53','2016-11-04 14:24:53'),(8,3,3,'224_20161104122506.jpg','2016-11-04 14:25:06','2016-11-04 14:25:06'),(9,3,4,'225_20161104122526.jpg','2016-11-04 14:25:27','2016-11-04 14:25:27'),(10,4,0,'vero_20161104122733.jpg','2016-11-04 14:27:34','2016-11-04 14:27:34'),(11,5,1,'uol_20161104122826.jpg','2016-11-04 14:28:27','2016-11-04 14:28:27'),(12,5,2,'uol2_20161104122909.jpg','2016-11-04 14:29:09','2016-11-04 14:29:09'),(13,6,2,'mini2_20161104123048.jpg','2016-11-04 14:30:48','2016-11-04 14:30:48'),(14,6,3,'mini3_20161104123048.jpg','2016-11-04 14:30:49','2016-11-04 14:30:49'),(15,6,1,'minima_20161104123048.jpg','2016-11-04 14:30:49','2016-11-04 14:30:49'),(16,7,0,'kaza1_20161104123234.jpg','2016-11-04 14:32:34','2016-11-04 14:32:34'),(17,8,0,'agora_20161104123336.jpg','2016-11-04 14:33:37','2016-11-04 14:33:37'),(18,9,0,'gazeta_20161104123508.jpg','2016-11-04 14:35:09','2016-11-04 14:35:09'),(20,10,0,'decorar2_20161104123721.jpg','2016-11-04 14:37:21','2016-11-04 14:37:21'),(21,10,0,'decorar3_20161104123722.jpg','2016-11-04 14:37:22','2016-11-04 14:37:22'),(22,11,0,'dec1_20161104123951.jpg','2016-11-04 14:39:52','2016-11-04 14:39:52'),(23,11,0,'dec2_20161104123952.jpg','2016-11-04 14:39:53','2016-11-04 14:39:53'),(24,11,0,'dec3_20161104123952.jpg','2016-11-04 14:39:53','2016-11-04 14:39:53'),(25,11,0,'dec4_20161104123953.jpg','2016-11-04 14:39:53','2016-11-04 14:39:53'),(26,11,0,'dec5_20161104123953.jpg','2016-11-04 14:39:54','2016-11-04 14:39:54'),(27,11,0,'dec6_20161104123954.jpg','2016-11-04 14:39:54','2016-11-04 14:39:54'),(28,12,1,'d2_20161104124846.jpg','2016-11-04 14:48:47','2016-11-04 14:48:47'),(29,12,2,'d3_20161104124847.jpg','2016-11-04 14:48:47','2016-11-04 14:48:47'),(30,12,5,'d4_20161104124847.jpg','2016-11-04 14:48:47','2016-11-04 14:48:47'),(31,12,6,'d5_20161104124847.jpg','2016-11-04 14:48:48','2016-11-04 14:48:48'),(32,12,7,'d6_20161104124848.jpg','2016-11-04 14:48:48','2016-11-04 14:48:48'),(33,12,8,'d7_20161104124848.jpg','2016-11-04 14:48:48','2016-11-04 14:48:48'),(34,12,9,'d8_20161104124848.jpg','2016-11-04 14:48:49','2016-11-04 14:48:49'),(35,12,10,'d9_20161104124848.jpg','2016-11-04 14:48:49','2016-11-04 14:48:49'),(36,12,3,'d10_20161104124849.jpg','2016-11-04 14:48:49','2016-11-04 14:48:49'),(37,12,4,'d11_20161104124849.jpg','2016-11-04 14:48:49','2016-11-04 14:48:49'),(38,13,0,'c1_20161104124954.jpg','2016-11-04 14:49:55','2016-11-04 14:49:55'),(39,14,0,'co2_20161104125211.jpg','2016-11-04 14:52:12','2016-11-04 14:52:12'),(40,14,0,'co3_20161104125211.jpg','2016-11-04 14:52:12','2016-11-04 14:52:12'),(41,14,0,'co4_20161104125211.jpg','2016-11-04 14:52:12','2016-11-04 14:52:12'),(42,14,0,'co5_20161104125212.jpg','2016-11-04 14:52:12','2016-11-04 14:52:12'),(43,14,0,'co6_20161104125212.jpg','2016-11-04 14:52:12','2016-11-04 14:52:12'),(44,14,0,'co7_20161104125212.jpg','2016-11-04 14:52:13','2016-11-04 14:52:13'),(45,15,0,'x2_20161104125516.jpg','2016-11-04 14:55:17','2016-11-04 14:55:17'),(46,16,0,'ponto_20161104125623.jpg','2016-11-04 14:56:23','2016-11-04 14:56:23'),(47,17,0,'casa_20161104125910.jpg','2016-11-04 14:59:11','2016-11-04 14:59:11'),(48,18,0,'an2_20161104130010.jpg','2016-11-04 15:00:11','2016-11-04 15:00:11'),(49,19,0,'anv2_20161104130116.jpg','2016-11-04 15:01:17','2016-11-04 15:01:17'),(50,20,0,'a2_20161104130212.jpg','2016-11-04 15:02:13','2016-11-04 15:02:13'),(51,21,0,'miami-home-decor-page-2_20161123132402.jpg','2016-11-23 15:24:03','2016-11-23 15:24:03'),(52,21,0,'miami-home-decor-page-3_20161123132402.jpg','2016-11-23 15:24:03','2016-11-23 15:24:03'),(53,21,0,'miami-home-decor-page-1_20161123132402.jpg','2016-11-23 15:24:03','2016-11-23 15:24:03'),(54,22,0,'casa-vogue_20161123133349.jpg','2016-11-23 15:33:49','2016-11-23 15:33:49'),(55,23,0,'anuario-stock-pagina-1_20161123134150.jpg','2016-11-23 15:41:51','2016-11-23 15:41:51'),(56,23,0,'anuario-stock-pagina-2_20161123134151.jpg','2016-11-23 15:41:51','2016-11-23 15:41:51'),(57,23,0,'anuario-stock-pagina-3_20161123134151.jpg','2016-11-23 15:41:52','2016-11-23 15:41:52'),(58,23,0,'anuario-stock-pagina-4_20161123134151.jpg','2016-11-23 15:41:52','2016-11-23 15:41:52');
/*!40000 ALTER TABLE `midia_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_10_21_180158_create_newsletter_table',1),('2016_10_21_183855_create_servicos_table',1),('2016_10_21_185443_create_servicos_abertura_table',1),('2016_10_25_004220_create_banners_table',1),('2016_10_25_004425_create_home_table',1),('2016_10_25_004629_create_a_infinity_table',1),('2016_10_25_004712_create_midia_table',1),('2016_10_25_141750_create_projetos_table',1),('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_10_21_180158_create_newsletter_table',1),('2016_10_21_183855_create_servicos_table',1),('2016_10_21_185443_create_servicos_abertura_table',1),('2016_10_25_004220_create_banners_table',1),('2016_10_25_004425_create_home_table',1),('2016_10_25_004629_create_a_infinity_table',1),('2016_10_25_004712_create_midia_table',1),('2016_10_25_141750_create_projetos_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projetos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conceito` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conceito_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desafios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desafios_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`),
  CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,3,3,'dol-7899_20161104114945.jpg','P4 ENGENHARIA ','','p4-engenharia','São Paulo - SP','','200m²','','2014','Moderno e Contempor&acirc;neo','','Atender e entender o conceito da empresa em um momento de expans&atilde;o. A segmenta&ccedil;&atilde;o das &aacute;reas em um open space tamb&eacute;m foi um dos principais pilares trabalhados.&nbsp;','','2016-11-01 15:39:53','2016-11-23 14:27:30'),(2,1,1,'img-2157_20161123123405.jpg','SÃO PEDRO ','','sao-pedro','GUARUJÁ - SP','','1.200m²','','2015','Moderno e Contempor&acirc;neo','','Integra&ccedil;&atilde;o de ambientes e a natureza exuberante do local.','','2016-11-01 15:41:24','2016-11-30 20:13:25'),(3,3,1,'img-0832_20161123123832.jpg','INFINITY BUSINESS','','infinity-business','ALPHAVILLE - SP','','1500m²','','2016','Moderno e Contempor&acirc;neo.','','Projeto de escrit&oacute;rios compartilhados e espa&ccedil;o para reuni&otilde;es e eventos, dentro de um ambiente moderno e que atenda os mais divernos nichos de empresas.','','2016-11-23 14:38:33','2016-11-23 14:38:33'),(4,1,5,'img-0875_20161123124334.jpg','CAMPO BELO','','campo-belo','SÃO PAULO - SP','','200m²','','2013','Moderno e Contempor&acirc;neo.','','Projeto com p&eacute; direito duplo e ambiente que atenda as necessidades de uma fam&iacute;lia com crian&ccedil;as.','','2016-11-23 14:43:34','2016-11-23 14:43:34'),(5,3,6,'img-4822_20161123124538.jpg','INFINITY SPACES','','infinity-spaces','TAMBORÉ - SP','','200m²','','2011','Moderno e Funcional.','','Funcionalidade, integra&ccedil;&atilde;o e modernidade.','','2016-11-23 14:45:39','2016-11-23 14:45:39'),(6,1,4,'trump-towers-sunny-isles-beach-36_20161123124801.jpg','SUNNY ISLES','','sunny-isles','FLÓRIDA - EUA','','300m²','','2013','Moderno e Contempor&acirc;neo','','Integra&ccedil;&atilde;o, funcionalidade e otimiza&ccedil;&atilde;o das exuberantes vistas para o mar local.','','2016-11-23 14:48:06','2016-11-23 14:48:06'),(7,3,5,'img-0868_20161123125151.jpg','PANTOS DO BRASIL','','pantos-do-brasil','TAMBORÉ - SP','','500m²','','2014','Moderno e Funcional','','Integra&ccedil;&atilde;o dentro de um ambiente segmentado e otimiza&ccedil;&atilde;o de espa&ccedil;o.','','2016-11-23 14:52:00','2016-11-23 14:52:00'),(8,1,6,'img-5785_20161123130320.jpg','IPORANGA','','iporanga','GUARUJÁ - SP','','400m²','','2010','Moderno e Contempor&acirc;neo','','Integra&ccedil;&atilde;o e Funcionalidade.','','2016-11-23 15:03:20','2016-11-23 15:03:20'),(9,1,3,'img-0607_20161123130522.jpg','ALPHASÍTIO','','alphasitio','TAMBORÉ - SP','','200m²','','2015','Moderno e Contempor&acirc;neo','','Integra&ccedil;&atilde;o de Ambientes.','','2016-11-23 15:05:22','2016-11-23 15:05:22'),(10,3,4,'dol-7821_20161123130847.JPG','SAPPHIRE','','sapphire','TAMBORÉ - SP','','100m²','','2014','Moderno e Funcional','','Otimiza&ccedil;&atilde;o de espa&ccedil;o e funcionalidade.','','2016-11-23 15:08:53','2016-11-23 15:08:53'),(11,1,2,'img-9717_20161123180730.jpg','RESIDENCIAL ALPHAVILLE II','','residencial-alphaville-ii','ALPHAVILLE - SP','','1200m²','','2010','Moderno e Contempor&acirc;neo','','Integra&ccedil;&atilde;o e Funcionalidade','','2016-11-23 20:07:31','2016-11-23 20:07:31'),(12,3,2,'ante-sala-2_20161123191710.jpg','UNICA SERVICE','','unica-service','TAMBORÉ - SP','','500m²','','2010','Moderno e Funcional.','','Integra&ccedil;&atilde;o de ambientes.','','2016-11-23 21:17:11','2016-11-23 21:17:11'),(13,3,0,'img-4919_20161202132337.jpg','INFINITY SPACES 2','','infinity-spaces-2','ALPHAVILLE - SP','','200m²','','2015','Moderno e Contempor&acirc;neo','','*','','2016-12-02 15:23:38','2016-12-02 15:23:38'),(14,1,0,'foto-6_20161202184433.jpg','FAZENDA BOA VISTA ','','fazenda-boa-vista','PORTO FELIZ  - SP','','500m²','','2014','Moderno e Contempor&acirc;neo','','*','','2016-12-02 20:43:22','2016-12-02 20:44:34');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_categorias`
--

DROP TABLE IF EXISTS `projetos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_categorias`
--

LOCK TABLES `projetos_categorias` WRITE;
/*!40000 ALTER TABLE `projetos_categorias` DISABLE KEYS */;
INSERT INTO `projetos_categorias` VALUES (1,1,'residencial','','residencial','2016-11-01 15:39:03','2016-11-01 15:39:03'),(3,3,'corporativo','','corporativo','2016-11-01 15:39:09','2016-11-01 15:39:09');
/*!40000 ALTER TABLE `projetos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens_destaque`
--

DROP TABLE IF EXISTS `projetos_imagens_destaque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens_destaque` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_destaque_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_destaque_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens_destaque`
--

LOCK TABLES `projetos_imagens_destaque` WRITE;
/*!40000 ALTER TABLE `projetos_imagens_destaque` DISABLE KEYS */;
INSERT INTO `projetos_imagens_destaque` VALUES (7,2,4,'img-2182_20161123123428.jpg','2016-11-23 14:34:29','2016-11-23 14:34:29'),(8,2,3,'img-2112_20161123123431.jpg','2016-11-23 14:34:32','2016-11-23 14:34:32'),(9,2,6,'img-2308_20161123123432.jpg','2016-11-23 14:34:33','2016-11-23 14:34:33'),(10,2,5,'img-2236_20161123123441.jpg','2016-11-23 14:34:53','2016-11-23 14:34:53'),(11,2,7,'img-1876_20161123123457.jpg','2016-11-23 14:35:11','2016-11-23 14:35:11'),(12,2,2,'img-2104_20161123123504.jpg','2016-11-23 14:35:20','2016-11-23 14:35:20'),(13,2,1,'img-2027_20161123123502.jpg','2016-11-23 14:35:21','2016-11-23 14:35:21'),(14,3,0,'img-9086_20161123123909.jpg','2016-11-23 14:39:27','2016-11-23 14:39:27'),(15,3,0,'img-2479_20161123123919.jpg','2016-11-23 14:39:41','2016-11-23 14:39:41'),(16,3,0,'img-2491b_20161123123919.jpg','2016-11-23 14:39:41','2016-11-23 14:39:41'),(17,3,0,'img-2548b_20161123123919.jpg','2016-11-23 14:39:41','2016-11-23 14:39:41'),(18,4,1,'img-0840_20161123124347.jpg','2016-11-23 14:43:48','2016-11-23 14:43:48'),(19,4,2,'img-0885_20161123124347.jpg','2016-11-23 14:43:49','2016-11-23 14:43:49'),(20,4,3,'img-0607_20161123124347.jpg','2016-11-23 14:43:49','2016-11-23 14:43:49'),(21,4,4,'img-0663_20161123124347.jpg','2016-11-23 14:43:49','2016-11-23 14:43:49'),(22,5,0,'img-4778_20161123124555.jpg','2016-11-23 14:45:56','2016-11-23 14:45:56'),(23,5,0,'img-4832_20161123124555.jpg','2016-11-23 14:45:57','2016-11-23 14:45:57'),(24,5,0,'img-4731_20161123124555.jpg','2016-11-23 14:45:57','2016-11-23 14:45:57'),(25,5,0,'img-4692_20161123124555.jpg','2016-11-23 14:45:57','2016-11-23 14:45:57'),(26,6,0,'trump-towers-sunny-isles-beach-80_20161123124834.jpg','2016-11-23 14:48:53','2016-11-23 14:48:53'),(27,6,0,'trump-towers-sunny-isles-beach-33_20161123124835.jpg','2016-11-23 14:48:53','2016-11-23 14:48:53'),(28,6,0,'trump-towers-sunny-isles-beach-3_20161123124841.jpg','2016-11-23 14:48:59','2016-11-23 14:48:59'),(29,6,0,'trump-towers-sunny-isles-beach-25_20161123124840.jpg','2016-11-23 14:49:00','2016-11-23 14:49:00'),(30,7,0,'img-0905_20161123125421.jpg','2016-11-23 14:54:37','2016-11-23 14:54:37'),(31,7,0,'img-0912_20161123125430.jpg','2016-11-23 14:54:51','2016-11-23 14:54:51'),(32,7,0,'img-0888_20161123125432.jpg','2016-11-23 14:54:51','2016-11-23 14:54:51'),(33,7,0,'img-0903_20161123125431.jpg','2016-11-23 14:54:51','2016-11-23 14:54:51'),(34,8,0,'img-5877b_20161123130332.jpg','2016-11-23 15:03:32','2016-11-23 15:03:32'),(35,8,0,'img-5954_20161123130332.jpg','2016-11-23 15:03:32','2016-11-23 15:03:32'),(36,8,0,'img-5814_20161123130332.jpg','2016-11-23 15:03:32','2016-11-23 15:03:32'),(37,8,0,'img-5795_20161123130332.jpg','2016-11-23 15:03:32','2016-11-23 15:03:32'),(38,9,0,'img-0502_20161123130537.jpg','2016-11-23 15:05:38','2016-11-23 15:05:38'),(39,9,0,'img-0724_20161123130537.jpg','2016-11-23 15:05:38','2016-11-23 15:05:38'),(40,9,0,'img-0809_20161123130537.jpg','2016-11-23 15:05:39','2016-11-23 15:05:39'),(41,9,0,'img-0571_20161123130542.jpg','2016-11-23 15:05:43','2016-11-23 15:05:43'),(42,10,0,'dol-7853_20161123130930.JPG','2016-11-23 15:09:52','2016-11-23 15:09:52'),(43,10,0,'dol-7810_20161123130933.JPG','2016-11-23 15:09:55','2016-11-23 15:09:55'),(44,11,8,'img-1931_20161130181020.jpg','2016-11-30 20:10:21','2016-11-30 20:10:21'),(45,11,2,'img-1793_20161130181020.jpg','2016-11-30 20:10:21','2016-11-30 20:10:21'),(46,11,1,'img-1708_20161130181020.jpg','2016-11-30 20:10:21','2016-11-30 20:10:21'),(47,11,5,'img-3432_20161130181020.jpg','2016-11-30 20:10:21','2016-11-30 20:10:21'),(48,11,7,'img-3490_20161130181020.jpg','2016-11-30 20:10:21','2016-11-30 20:10:21'),(49,11,9,'img-1874_20161130181020.jpg','2016-11-30 20:10:21','2016-11-30 20:10:21'),(50,11,3,'img-9500_20161130181022.jpg','2016-11-30 20:10:22','2016-11-30 20:10:22'),(51,11,6,'img-9717_20161130181022.jpg','2016-11-30 20:10:22','2016-11-30 20:10:22'),(52,11,4,'img-9593_20161130181022.jpg','2016-11-30 20:10:22','2016-11-30 20:10:22'),(53,12,0,'ante-sala-1_20161130181426.jpg','2016-11-30 20:14:27','2016-11-30 20:14:27'),(54,12,0,'sala-de-reuniao-1_20161130181426.jpg','2016-11-30 20:14:27','2016-11-30 20:14:27'),(55,12,0,'ante-sala-2_20161130181426.jpg','2016-11-30 20:14:27','2016-11-30 20:14:27'),(56,12,0,'cyber-cafe_20161130181426.jpg','2016-11-30 20:14:27','2016-11-30 20:14:27'),(57,12,0,'sala-de-reuniao-2_20161130181426.jpg','2016-11-30 20:14:27','2016-11-30 20:14:27'),(60,1,0,'dol-7877_20161130181601.JPG','2016-11-30 20:16:14','2016-11-30 20:16:14'),(61,1,0,'dol-7872_20161130181601.JPG','2016-11-30 20:16:15','2016-11-30 20:16:15'),(62,1,0,'dol-7908_20161130181701.JPG','2016-11-30 20:17:12','2016-11-30 20:17:12'),(63,1,0,'dol-7869_20161130181701.JPG','2016-11-30 20:17:12','2016-11-30 20:17:12'),(64,13,0,'img-4849_20161202132607.jpg','2016-12-02 15:26:08','2016-12-02 15:26:08'),(65,13,0,'img-4832_20161202132607.jpg','2016-12-02 15:26:08','2016-12-02 15:26:08'),(66,13,0,'img-4862_20161202132608.jpg','2016-12-02 15:26:08','2016-12-02 15:26:08'),(68,13,0,'img-4912_20161202132608.jpg','2016-12-02 15:26:09','2016-12-02 15:26:09'),(69,13,0,'img-4793_20161202132608.jpg','2016-12-02 15:26:09','2016-12-02 15:26:09'),(70,13,0,'img-4931_20161202132609.jpg','2016-12-02 15:26:10','2016-12-02 15:26:10'),(71,14,2,'foto-2_20161202184341.jpg','2016-12-02 20:43:45','2016-12-02 20:43:45'),(72,14,5,'foto-3_20161202184342.jpg','2016-12-02 20:43:46','2016-12-02 20:43:46'),(73,14,3,'foto-4_20161202184345.jpg','2016-12-02 20:43:49','2016-12-02 20:43:49'),(74,14,6,'foto-7_20161202184347.jpg','2016-12-02 20:43:51','2016-12-02 20:43:51'),(75,14,4,'foto-5_20161202184348.jpg','2016-12-02 20:43:52','2016-12-02 20:43:52'),(77,14,1,'foto-1_20161202184453.jpg','2016-12-02 20:44:56','2016-12-02 20:44:56');
/*!40000 ALTER TABLE `projetos_imagens_destaque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,1,'ESTUDO DE VIABILIDADE','','ico-estudosviabilidade_20161101153540.png','<p>Pesquisa de mercado</p>\r\n\r\n<p>Adequa&ccedil;&atilde;o as necessidades dos clientes</p>\r\n','','2016-11-01 15:35:40','2016-11-04 13:52:31'),(2,2,'PROJETOS','','ico-projetos_20161101153557.png','<p>Cria&ccedil;&atilde;o de layouts</p>\r\n\r\n<p>Projeto executivo</p>\r\n\r\n<p>Especifica&ccedil;&atilde;o</p>\r\n','','2016-11-01 15:35:57','2016-11-04 13:52:56'),(3,3,'GERENCIAMENTO DE PROJETOS','','ico-credenciamento_20161101153631.png','<p>Precifica&ccedil;&atilde;o</p>\r\n\r\n<p>Planejamento de obra</p>\r\n\r\n<p>Cronograma f&iacute;sico financeiro</p>\r\n\r\n<p>Relat&oacute;rios de obra</p>\r\n','','2016-11-01 15:36:31','2016-11-04 13:27:55'),(4,4,'TURNKEY','','ico-turnkey_20161101153651.png','<p>Flexibilidade&nbsp;</p>\r\n\r\n<p>Agilidade</p>\r\n\r\n<p>Prazo</p>\r\n\r\n<p>Budget&nbsp;</p>\r\n','','2016-11-01 15:36:51','2016-11-04 13:39:27'),(5,5,'EXECUÇÃO DE OBRA','','ico-execucaoobra_20161101153701.png','<p>Realiza&ccedil;&atilde;o dos servi&ccedil;os para entrega do&nbsp;<br />\r\nespa&ccedil;o finalizado&nbsp;</p>\r\n','','2016-11-01 15:37:01','2016-11-04 13:39:49'),(6,6,'PRODUÇÃO','','ico-producao_20161101153731.png','<p>Ambienta&ccedil;&atilde;o</p>\r\n\r\n<p>Espa&ccedil;o pronto para eventos e fotografia</p>\r\n','','2016-11-01 15:37:31','2016-11-04 13:40:18');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos_abertura`
--

DROP TABLE IF EXISTS `servicos_abertura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos_abertura` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos_abertura`
--

LOCK TABLES `servicos_abertura` WRITE;
/*!40000 ALTER TABLE `servicos_abertura` DISABLE KEYS */;
INSERT INTO `servicos_abertura` VALUES (1,'AGILIDADE E PRECISÃO = NOSSOS SERVIÇOS','','<p>Da concep&ccedil;&atilde;o at&eacute; seu espa&ccedil;o pronto.</p>\r\n\r\n<p>Executamos todas as etapas de seu projeto.</p>\r\n','','capa-2-residenciais_20161123195546.jpg','capa-corporativos_20161123195323.jpg',NULL,'2016-11-23 21:55:46');
/*!40000 ALTER TABLE `servicos_abertura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$LuP5EL2T6sTfPBhkeZXjp.GEE6ZqrgZfRtGKVpNAlvqsWi6SoCJ26','imhUzb2Fgu5ZFfiG5IDk1J7pRH5yfxftoXn0tanDCdE1oWfKxcfR6973a4QM',NULL,'2016-11-01 15:28:55'),(2,'infinity','infinity@infinity.com.br','$2y$10$75J8nA7LA2w0g4yfUFFWIO.LPXc3fqdgctr8/gBWmj5TiPx33yp3K','ZuRELILpL4E7Es5ehDMpbgPqF9pU5t9RlsSQfNoZMioHb7Ua4qAcMMZCxWbd','2016-11-04 15:27:46','2016-11-04 18:36:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-06 13:59:41
