<?php

    return [

        'nav' => [
            'infinity' => 'a Infinity',
            'servicos' => 'serviços',
            'projetos' => 'projetos',
            'clientes' => 'clientes',
            'imoveis'  => 'imóveis',
            'midia'    => 'mídia',
            'contato'  => 'contato'
        ],

        'footer' => [
            'cadastre'     => 'Cadastre-se para receber novidades',
            'nome'         => 'nome',
            'email'        => 'e-mail',
            'nome_req'     => 'preencha seu nome',
            'email_req'    => 'insira um endereço de e-mail',
            'email_val'    => 'insira um endereço de e-mail válido',
            'email_unique' => 'o e-mail inserido já está cadastrado',
            'sucesso'      => 'cadastro efetuado com sucesso!',
            'direitos'     => 'Todos os direitos reservados.',
            'criacao'      => 'Criação de sites',
            'trupe'        => 'Trupe Agência Criativa'
        ],

        'clientes' => 'CLIENTES INFINITY',

        'midia' => [
            'anterior' => 'Anterior',
            'voltar'   => 'Voltar',
            'proximo'  => 'Próximo'
        ],

        'projetos' => [
            'conceito'        => 'CONCEITO',
            'desafios'        => 'DESAFIOS',
            'maisprojetos'    => 'MAIS PROJETOS',
            'perspectiva'     => 'VER PERSPECTIVA 3D',
            'caracteristicas' => 'Características',
            'contate'         => 'CONTATE-NOS'
        ],

        'contato' => [
            'nome'     => 'nome',
            'email'    => 'e-mail',
            'telefone' => 'telefone',
            'mensagem' => 'mensagem',
            'enviar'   => 'ENVIAR',
            'sucesso'  => 'mensagem enviada com sucesso!',
            'erro'     => 'preencha todos os campos corretamente'
        ],

        'home' => [
            'servicos'    => 'NOSSOS SERVIÇOS',
            'sabermais'   => 'SABER MAIS',
            'faleconosco' => 'FALE CONOSCO E TIRE SUAS DÚVIDAS',
            'contato'     => 'CONTATO'
        ],

        'infinity' => [
            'pessoaschave' => 'PESSOAS-CHAVE'
        ],

        'banco' => [
            'titulo'              => 'titulo',
            'endereco'            => 'endereco',
            'descricao'           => 'descricao',
            'texto'               => 'texto',
            'chamada_titulo'      => 'chamada_titulo',
            'chamada_texto'       => 'chamada_texto',
            'servicos_titulo'     => 'servicos_titulo',
            'servicos_texto'      => 'servicos_texto',
            'projetos_titulo'     => 'projetos_titulo',
            'projetos_texto'      => 'projetos_texto',
            'pessoas_chave_texto' => 'pessoas_chave_texto',
            'local'               => 'local',
            'area'                => 'area',
            'conceito'            => 'conceito',
            'desafios'            => 'desafios',
            'caracteristicas'     => 'caracteristicas',
        ]

    ];
