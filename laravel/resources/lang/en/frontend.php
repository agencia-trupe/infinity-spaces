<?php

    return [

        'nav' => [
            'infinity' => 'Infinity',
            'servicos' => 'services',
            'projetos' => 'projects',
            'clientes' => 'clients',
            'imoveis'  => 'properties',
            'midia'    => 'media',
            'contato'  => 'contact'
        ],

        'footer' => [
            'cadastre'     => 'Subscribe to receive our news',
            'nome'         => 'name',
            'email'        => 'e-mail',
            'nome_req'     => 'fill in your name',
            'email_req'    => 'fill in your e-mail',
            'email_val'    => 'insert a valid e-mail address',
            'email_unique' => 'this e-mail is already registered',
            'sucesso'      => 'e-mail registered successfully!',
            'direitos'     => 'All rights reserved.',
            'criacao'      => 'Sites:',
            'trupe'        => 'Trupe Agência Criativa'
        ],

        'clientes' => 'INFINITY CLIENTS',

        'midia' => [
            'anterior' => 'Previous',
            'voltar'   => 'Back',
            'proximo'  => 'Next'
        ],

        'projetos' => [
            'conceito'        => 'CONCEPT',
            'desafios'        => 'CHALLENGES',
            'maisprojetos'    => 'MORE PROJECTS',
            'perspectiva'     => 'SEE 3D PERSPECTIVE',
            'caracteristicas' => 'Features',
            'contate'         => 'CONTACT US'
        ],

        'contato' => [
            'nome'     => 'name',
            'email'    => 'e-mail',
            'telefone' => 'phone',
            'mensagem' => 'message',
            'enviar'   => 'SEND',
            'sucesso'  => 'message sent successfully!',
            'erro'     => 'all fields are required'
        ],

        'home' => [
            'servicos'    => 'OUR SERVICES',
            'sabermais'   => 'LEARN MORE',
            'faleconosco' => 'CONTACT US',
            'contato'     => 'CONTACT'
        ],

        'infinity' => [
            'pessoaschave' => 'KEY PEOPLE'
        ],

        'banco' => [
            'titulo'              => 'titulo_en',
            'endereco'            => 'endereco_en',
            'descricao'           => 'descricao_en',
            'texto'               => 'texto_en',
            'chamada_titulo'      => 'chamada_titulo_en',
            'chamada_texto'       => 'chamada_texto_en',
            'servicos_titulo'     => 'servicos_titulo_en',
            'servicos_texto'      => 'servicos_texto_en',
            'projetos_titulo'     => 'projetos_titulo_en',
            'projetos_texto'      => 'projetos_texto_en',
            'pessoas_chave_texto' => 'pessoas_chave_texto_en',
            'local'               => 'local_en',
            'area'                => 'area_en',
            'conceito'            => 'conceito_en',
            'desafios'            => 'desafios_en',
            'caracteristicas'     => 'caracteristicas_en',
        ]

    ];
