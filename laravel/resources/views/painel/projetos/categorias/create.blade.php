@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.projetos.categorias.store']) !!}

        @include('painel.projetos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
