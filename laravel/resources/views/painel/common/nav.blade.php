<ul class="nav navbar-nav">
	<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
    <li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.home.index') }}">Home</a>
    </li>
    <li @if(str_is('painel.a-infinity*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.a-infinity.index') }}">A Infinity</a>
    </li>
	<li @if(str_is('painel.servicos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.servicos.index') }}">Serviços</a>
	</li>
    <li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.projetos.index') }}">Projetos</a>
    </li>
    <li @if(str_is('painel.imoveis*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.imoveis.index') }}">Imóveis</a>
    </li>
    <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>
    <li @if(str_is('painel.midia*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.midia.index') }}">Mídia</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
