@extends('painel.common.template')

@section('content')

    <legend>
        <h2>A Infinity</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-infinity.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.a-infinity.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
