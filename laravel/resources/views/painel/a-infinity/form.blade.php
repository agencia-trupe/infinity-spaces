@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_6', 'Imagem 6') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->imagem_6) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_6', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo', 'Título') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [INGLÊS]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto', 'Texto') !!}
            {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'infinity']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto [INGLÊS]') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'infinity']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pessoas_chave_texto', 'Pessoas-chave Texto') !!}
            {!! Form::textarea('pessoas_chave_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pessoas_chave_texto_en', 'Pessoas-chave Texto [INGLÊS]') !!}
            {!! Form::textarea('pessoas_chave_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('pessoas_chave_foto', 'Pessoas-chave Foto') !!}
            <img src="{{ url('assets/img/a-infinity/'.$registro->pessoas_chave_foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('pessoas_chave_foto', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
