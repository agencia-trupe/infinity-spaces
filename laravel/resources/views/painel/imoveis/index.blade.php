@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Imóveis
            <a href="{{ route('painel.imoveis.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Imóvel</a>
        </h2>
    </legend>

    @if(!count($imoveis))
    <div class="alert alert-warning" role="alert">Nenhum imóvel cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="imoveis">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Capa</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($imoveis as $imovel)
            <tr class="tr-row" id="{{ $imovel->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>
                    {{ $imovel->titulo }}
                </td>
                <td><img src="{{ url('assets/img/imoveis/thumbs/'.$imovel->imagem) }}" alt="" style="width:100%;max-width:100px;height:auto;"></td>
                <td><a href="{{ route('painel.imoveis.imagens.index', $imovel->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.imoveis.destroy', $imovel->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.imoveis.edit', $imovel->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
