@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.servicos.index') }}" title="Voltar para Serviços" class="btn btn-sm btn-default">
        &larr; Voltar para Serviços
    </a>

    <legend>
        <h2><small>Serviços /</small> Abertura</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.servicos.abertura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.servicos.abertura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
