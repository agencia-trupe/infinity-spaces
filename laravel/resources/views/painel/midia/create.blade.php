@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mídia /</small> Adicionar Mídia</h2>
    </legend>

    {!! Form::open(['route' => 'painel.midia.store', 'files' => true]) !!}

        @include('painel.midia.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
