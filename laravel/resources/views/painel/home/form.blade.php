@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_titulo', 'Chamada Título') !!}
            {!! Form::text('chamada_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_titulo_en', 'Chamada Título [INGLÊS]') !!}
            {!! Form::text('chamada_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_texto', 'Chamada Texto') !!}
            {!! Form::textarea('chamada_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_texto_en', 'Chamada Texto [INGLÊS]') !!}
            {!! Form::textarea('chamada_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            <img src="{{ url('assets/img/home/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            <img src="{{ url('assets/img/home/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            <img src="{{ url('assets/img/home/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('servicos_titulo', 'Serviços Título') !!}
            {!! Form::text('servicos_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('servicos_titulo_en', 'Serviços Título [INGLÊS]') !!}
            {!! Form::text('servicos_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('servicos_texto', 'Serviços Texto') !!}
            {!! Form::textarea('servicos_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('servicos_texto_en', 'Serviços Texto [INGLÊS]') !!}
            {!! Form::textarea('servicos_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('projetos_titulo', 'Projetos Título') !!}
            {!! Form::text('projetos_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('projetos_titulo_en', 'Projetos Título [INGLÊS]') !!}
            {!! Form::text('projetos_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('projetos_texto', 'Projetos Texto') !!}
            {!! Form::textarea('projetos_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('projetos_texto_en', 'Projetos Texto [INGLÊS]') !!}
            {!! Form::textarea('projetos_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('projetos_imagem', 'Projetos Imagem') !!}
    <img src="{{ url('assets/img/home/'.$registro->projetos_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('projetos_imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
