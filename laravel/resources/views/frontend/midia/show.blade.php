@extends('frontend.common.template')

@section('content')

    <div class="midia-show">
        <div class="center">
            @foreach($midia->imagens as $imagem)
            <img src="{{ asset('assets/img/midia/imagens/'.$imagem->imagem) }}" alt="">
            @endforeach

            <div class="links">
                @if($anterior)
                <a href="{{ route('midia.show', $anterior->slug) }}">&laquo; {{ trans('frontend.midia.anterior') }}</a>
                @endif

                <a href="{{ route('midia') }}">{{ trans('frontend.midia.voltar') }}</a>

                @if($proximo)
                <a href="{{ route('midia.show', $proximo->slug) }}">{{ trans('frontend.midia.proximo') }} &raquo;</a>
                @endif
            </div>
        </div>
    </div>

@endsection
