@extends('frontend.common.template')

@section('content')

    <div class="midia-index">
        <div class="center">
            @foreach($publicacoes as $publicacao)
            <a href="{{ route('midia.show', $publicacao->slug) }}">
                <div class="imagem">
                    <img src="{{ asset('assets/img/midia/'.$publicacao->capa) }}" alt="{{ $publicacao->{trans('frontend.banco.titulo')} }}">
                </div>
                <span>{{ Tools::formataData($publicacao->data, app()->getLocale()) }}</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
