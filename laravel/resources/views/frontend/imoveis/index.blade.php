@extends('frontend.common.template')

@section('content')

    <div class="projetos-index">
        <div class="center">
            @foreach($imoveis as $imovel)
            <a href="{{ route('imoveis.show', [$imovel->slug]) }}">
                <img src="{{ asset('assets/img/imoveis/thumbs/'.$imovel->imagem) }}">
                <div class="overlay">
                    <p>
                        {{ $imovel->{trans('frontend.banco.titulo')} }}
                        <span>{{ $imovel->{trans('frontend.banco.local')} }}</span>
                    </p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
