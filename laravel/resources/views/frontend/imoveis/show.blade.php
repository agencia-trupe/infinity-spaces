@extends('frontend.common.template')

@section('content')

    <div class="imoveis-show">
        <div class="center">
            <div class="imovel-informacoes">
                <h2>{{ $imovel->{trans('frontend.banco.titulo')} }}</h2>
                <p>{{ $imovel->{trans('frontend.banco.local')} }}</p>

                <p class="caracteristicas">
                    <span class="titulo">
                        {{ trans('frontend.projetos.caracteristicas') }}:
                    </span>
                    {!! $imovel->{trans('frontend.banco.caracteristicas')} !!}
                </p>

                @if($imovel->link_360)
                <div class="perspectiva">
                    <a href="{{ Tools::parseLink($imovel->link_360) }}" class="btn-perspectiva" target="_blank">
                        {{ trans('frontend.projetos.perspectiva') }} &raquo;
                    </a>
                </div>
                @endif

                <a href="{{ route('contato') }}" class="btn-contato">
                    {{ trans('frontend.projetos.contate') }} &raquo;
                </a>
            </div>

            <div class="imovel-imagens">
                @foreach($imovel->imagens as $imagem)
                    @if($imagem == $imovel->imagens->first())
                    <a href="{{ asset('assets/img/imoveis/imagens/'.$imagem->imagem) }}" class="destaque fancybox" rel="galeria" title="{{ $imovel->{trans('frontend.banco.titulo')} }}">
                        <img src="{{ asset('assets/img/imoveis/imagens/thumbs-destaque/'.$imagem->imagem) }}" alt="">
                    </a>
                    @else
                    <a href="{{ asset('assets/img/imoveis/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria" title="{{ $imovel->{trans('frontend.banco.titulo')} }}">
                        <img src="{{ asset('assets/img/imoveis/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                    </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection
