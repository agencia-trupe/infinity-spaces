@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" class="slide" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                <div class="center">
                    <span>{!! $banner->{trans('frontend.banco.titulo')} !!}</span>
                </div>
            </a>
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        <div class="center">
            <div class="chamada">
                <h2>{{ $home->{trans('frontend.banco.chamada_titulo')} }}</h2>
                <p>{!! $home->{trans('frontend.banco.chamada_texto')} !!}</p>
                <img src="{{ asset('assets/img/home/'.$home->imagem_1) }}" alt="">
                <img src="{{ asset('assets/img/home/'.$home->imagem_2) }}" alt="">
                <img src="{{ asset('assets/img/home/'.$home->imagem_3) }}" alt="">
            </div>

            <div class="servicos">
                <h2>{{ $home->{trans('frontend.banco.servicos_titulo')} }}</h2>
                <div class="servicos-wrapper">
                    @foreach($servicos as $servico)
                    <div class="servico">
                        <div class="imagem" style="background-image:url('{{ asset('assets/img/servicos/'.$servico->imagem) }}')"></div>
                        <p>{{ $servico->{trans('frontend.banco.titulo')} }}</p>
                    </div>
                    @endforeach
                </div>
                <p>{!! $home->{trans('frontend.banco.servicos_texto')} !!}</p>
                <a href="{{ route('servicos') }}" class="home-link">{{ trans('frontend.home.servicos') }} &raquo;</a>
            </div>

            <div class="projetos">
                <img src="{{ asset('assets/img/home/'.$home->projetos_imagem) }}" alt="">
                <h2>{{ $home->{trans('frontend.banco.projetos_titulo')} }}</h2>
                <p>{!! $home->{trans('frontend.banco.projetos_texto')} !!}</p>
                <a href="{{ route('projetos') }}" class="home-link">{{ trans('frontend.home.sabermais') }} &raquo;</a>
            </div>

            <div class="contato">
                <h2>{{ trans('frontend.home.faleconosco') }}</h2>
                <a href="{{ route('contato') }}" class="home-link">{{ trans('frontend.home.contato') }} &raquo;</a>
            </div>
        </div>
    </div>

@endsection
