@extends('frontend.common.template')

@section('content')

    <div class="projetos-categorias">
        <div class="center">
            @foreach($categorias as $cat)
            <a href="{{ route('projetos', $cat->slug) }}" @if($cat->slug === $categoria->slug) class="active" @endif>{{ $cat->{trans('frontend.banco.titulo')} }}</a>
            @endforeach
        </div>
    </div>

    <div class="projetos-show">
        <div class="center">
            <div class="main-box">
                <div class="info">
                    <h2>{{ $projeto->{trans('frontend.banco.titulo')} }}</h2>
                    <p>
                        {{ $projeto->{trans('frontend.banco.local')} }}<br>
                        {{ $projeto->{trans('frontend.banco.area')} }}<br>
                        {{ $projeto->ano }}
                    </p>
                </div>
                <div class="projeto-imagens-destaque">
                    @foreach($projeto->imagensDestaque as $imagem)
                    <img src="{{ asset('assets/img/projetos/imagens-destaque/thumbs-grande/'.$imagem->imagem) }}" alt="" data-id="{{ $imagem->id }}">
                    @endforeach

                    <a href="#" class="cycle-control cycle-prev">&xlarr;</a>
                    <a href="#" class="cycle-control cycle-next">&xrarr;</a>
                    <div class="cycle-pager"></div>

                    <a href="#" class="projetos-amplia-destaques"></a>
                </div>
            </div>

            @if($projeto->link_360)
            <div class="perspectiva">
                <a href="{{ Tools::parseLink($projeto->link_360) }}" class="btn-perspectiva" target="_blank">
                    {{ trans('frontend.projetos.perspectiva') }} &raquo;
                </a>
            </div>
            @endif

            @if($projeto->{trans('frontend.banco.conceito')})
            <div class="conceito">
                <h4>{{ trans('frontend.projetos.conceito') }}</h4>
                <p>{!! $projeto->{trans('frontend.banco.conceito')} !!}</p>
            </div>
            @endif

            @if($projeto->{trans('frontend.banco.desafios')})
            <div class="desafios">
                <h4>{{ trans('frontend.projetos.desafios') }}</h4>
                <p>{!! $projeto->{trans('frontend.banco.desafios')} !!}</p>
            </div>
            @endif

            <div class="projeto-imagens">
                @foreach($projeto->imagens as $imagem)
                <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria" title="{{ $projeto->{trans('frontend.banco.titulo')} }}">
                    <img src="{{ asset('assets/img/projetos/imagens/thumbs-altura/'.$imagem->imagem) }}" alt="">
                </a>
                @endforeach
            </div>

            <div class="mais-projetos">
                <h4>{{ $categoria->titulo }} &middot; {{ trans('frontend.projetos.maisprojetos') }}</h4>
                @foreach($maisProjetos as $outroProjeto)
                <a href="{{ route('projetos.show', [$outroProjeto->categoria->slug, $outroProjeto->slug]) }}">
                    <img src="{{ asset('assets/img/projetos/thumbs/'.$outroProjeto->imagem) }}">
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="hidden">
        @foreach($projeto->imagensDestaque as $imagem)
        <a href="{{ asset('assets/img/projetos/imagens-destaque/'.$imagem->imagem) }}" data-id="{{ $imagem->id }}" class="fancybox fancybox-destaque" title="{{ $projeto->titulo }}" rel="galeria-destaque"></a>
        @endforeach
    </div>

@endsection
