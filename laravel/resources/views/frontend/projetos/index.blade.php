@extends('frontend.common.template')

@section('content')

    <div class="projetos-categorias">
        <div class="center">
            @foreach($categorias as $cat)
            <a href="{{ route('projetos', $cat->slug) }}" @if($cat->slug === $categoria->slug) class="active" @endif>{{ $cat->{trans('frontend.banco.titulo')} }}</a>
            @endforeach
        </div>
    </div>

    <div class="projetos-index">
        <div class="center">
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
                <img src="{{ asset('assets/img/projetos/thumbs/'.$projeto->imagem) }}">
                <div class="overlay">
                    <p>
                        {{ $projeto->{trans('frontend.banco.titulo')} }}
                        <span>{{ $projeto->{trans('frontend.banco.local')} }}</span>
                    </p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
