<a href="{{ route('a-infinity') }}" @if(str_is('a-infinity', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.infinity') }}</a>
<a href="{{ route('servicos') }}" @if(str_is('servicos', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.servicos') }}</a>
<a href="{{ route('projetos') }}" @if(str_is('projetos*', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.projetos') }}</a>
<a href="{{ route('clientes') }}" @if(str_is('clientes*', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.clientes') }}</a>
<a href="{{ route('imoveis') }}" @if(str_is('imoveis*', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.imoveis') }}</a>
<a href="{{ route('midia') }}" @if(str_is('midia*', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.midia') }}</a>
<a href="http://arquitetura.design" target="_blank">blog</a>
<a href="{{ route('contato') }}" @if(str_is('contato', Route::currentRouteName())) class="active" @endif>{{ trans('frontend.nav.contato') }}</a>
