    <footer>
        <div class="center">
            <div class="newsletter">
                <p>{{ trans('frontend.footer.cadastre') }}</p>
                <form action="" id="form-newsletter" method="POST">
                    <input type="name" name="nome" id="newsletter_nome" placeholder="{{ trans('frontend.footer.nome') }}" required>
                    <input type="email" name="email" id="newsletter_email" placeholder="{{ trans('frontend.footer.email') }}" required>
                    <input type="submit" value="OK">
                    <div class="response-wrapper">
                        <span id="form-newsletter-response"></span>
                    </div>
                </form>
            </div>

            <div class="links">
                <div class="logo">
                    <img src="{{ asset('assets/img/layout/marca-infinityspaces-rodape.png') }}" alt="">
                    <div class="social">
                        @if($contato->instagram)
                        <a href="{{ $contato->instagram }}" class="instagram" target="_blank"></a>
                        @endif
                        @if($contato->facebook)
                        <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                        @endif
                    </div>
                </div>
                <div class="menu">
                    @include('frontend.common._nav')
                </div>
            </div>

            <div class="copyright">
                <p>© {{ date('Y') }} {{ config('site.name') }} <br> {{ trans('frontend.footer.direitos') }}</p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.footer.criacao') }}</a>:
                    <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.footer.trupe') }}</a>
                </p>
            </div>
        </div>
    </footer>
