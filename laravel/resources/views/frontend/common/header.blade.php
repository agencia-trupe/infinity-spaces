    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>

            <nav id="nav-desktop">
                @include('frontend.common._nav')
            </nav>

            <?php $locale = app()->getLocale() === 'pt' ? 'en' : 'pt'; ?>
            <a href="{{ route('lang', $locale) }}" class="lang lang-{{ $locale }}">
                {{ $locale === 'pt' ? 'Versão em Português' : 'English Version' }}
            </a>

            <div class="social">
                @if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="instagram" target="_blank"></a>
                @endif
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                @endif
            </div>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
