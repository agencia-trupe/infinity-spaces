@extends('frontend.common.template')

@section('content')

    <div class="infinity">
        <div class="imagens">
            @foreach(range(1,6) as $i)
            <img src="{{ asset('assets/img/a-infinity/'.$infinity->{'imagem_'.$i}) }}" alt="">
            @endforeach
        </div>

        <div class="center">
            <div class="left">
                <h2>{{ $infinity->{trans('frontend.banco.titulo')} }}</h2>
                <div class="texto">
                    {!! $infinity->{trans('frontend.banco.texto')} !!}
                </div>
                <img src="{{ asset('assets/img/layout/img-poltrona.png') }}" alt="">
            </div>

            <div class="right">
                <h3>{{ trans('frontend.infinity.pessoaschave') }}</h3>
                <div class="texto">
                    <img src="{{ asset('assets/img/a-infinity/'.$infinity->pessoas_chave_foto) }}" alt="">
                    {!! $infinity->{trans('frontend.banco.pessoas_chave_texto')} !!}
                </div>
            </div>
        </div>
    </div>

@endsection
