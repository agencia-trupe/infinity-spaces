@extends('frontend.common.template')

@section('content')

    <div class="clientes">
        <div class="center">
            <h2>{{ trans('frontend.clientes') }}</h2>

            <div class="clientes-imagens">
                @foreach($clientes as $cliente)
                <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
