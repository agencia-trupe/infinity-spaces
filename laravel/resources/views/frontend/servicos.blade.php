@extends('frontend.common.template')

@section('content')

    <div class="servicos">
        <div class="center">
            <h2>{{ $abertura->{trans('frontend.banco.titulo')} }}</h2>
            <div class="texto">
                {!! $abertura->{trans('frontend.banco.texto')} !!}
            </div>
            <div class="imagens">
                <img src="{{ asset('assets/img/servicos-abertura/'.$abertura->imagem_1) }}" alt="">
                <img src="{{ asset('assets/img/servicos-abertura/'.$abertura->imagem_2) }}" alt="">
            </div>

            <div class="servicos">
                @foreach($servicos as $servico)
                <div class="servico">
                    <h4>{{ $servico->{trans('frontend.banco.titulo')} }}</h4>
                    <div class="imagem" style="background-image:url('{{ asset('assets/img/servicos/'.$servico->imagem) }}')"></div>
                    <div class="descricao">
                        {!! $servico->{trans('frontend.banco.descricao')} !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
