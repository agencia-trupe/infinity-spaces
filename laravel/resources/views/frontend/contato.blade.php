@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="info">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->{trans('frontend.banco.endereco')} !!}</p>
            </div>

            <form action="" id="form-contato" method="POST">
                <div class="col">
                    <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                    <input type="email" name="email" id="email" placeholder="{{ trans('frontend.contato.email') }}" required>
                    <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                </div>
                <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                <div class="response-wrapper">
                    <div id="form-contato-response"></div>
                </div>
            </form>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
