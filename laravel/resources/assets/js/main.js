(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.envioNewsletter = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-newsletter-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: {
                nome: $('#newsletter_nome').val(),
                email: $('#newsletter_email').val()
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                var errorMsg = data.responseJSON.nome ? data.responseJSON.nome : data.responseJSON.email;
                $response.hide().text(errorMsg).fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.destaquesProjeto = function() {
        var $wrapper = $('.projeto-imagens-destaque');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>img',
            pagerTemplate: '<a href=#>{{slideNum}}</a>',
            timeout: 0
        });
    };

    App.ampliacaoProjeto = function() {
        var $handle = $('.projetos-amplia-destaques');

        $('.fancybox').fancybox({
            loop: false,
            padding: 0,
            maxHeight: '94%',
            afterLoad : function() {
                this.title = '<span>' + (this.title ? this.title : '') + '</span><span>&laquo; ' + (this.index + 1) + '/' + this.group.length + ' &raquo;</span>';
            },
        });

        $handle.click(function(e) {
            e.preventDefault();

            var el, id = $('.cycle-slide-active').data('id');
            if (id) {
                el = $('.fancybox-destaque[data-id=' + id + ']:eq(0)').first();
                el.click();
            }
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.destaquesProjeto();
        this.ampliacaoProjeto();
        $('#form-contato').on('submit', this.envioContato);
        $('#form-newsletter').on('submit', this.envioNewsletter);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
