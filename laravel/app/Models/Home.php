<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/home/ampliacao/'
            ],
            [
                'width'  => 360,
                'height' => 230,
                'path'   => 'assets/img/home/'
            ]
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/home/ampliacao/'
            ],
            [
                'width'  => 360,
                'height' => 230,
                'path'   => 'assets/img/home/'
            ]
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/home/ampliacao/'
            ],
            [
                'width'  => 360,
                'height' => 230,
                'path'   => 'assets/img/home/'
            ]
        ]);
    }

    public static function upload_projetos_imagem()
    {
        return CropImage::make('projetos_imagem', [
            'width'  => 1100,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
    }

}
