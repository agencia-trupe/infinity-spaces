<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AInfinity extends Model
{
    protected $table = 'a_infinity';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 300,
            'height' => 200,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 300,
            'height' => 200,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 300,
            'height' => 200,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 300,
            'height' => 200,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 300,
            'height' => 200,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

    public static function upload_imagem_6()
    {
        return CropImage::make('imagem_6', [
            'width'  => 300,
            'height' => 200,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

    public static function upload_pessoas_chave_foto()
    {
        return CropImage::make('pessoas_chave_foto', [
            'width'  => 150,
            'height' => null,
            'path'   => 'assets/img/a-infinity/'
        ]);
    }

}
