<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ServicosAbertura extends Model
{
    protected $table = 'servicos_abertura';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 345,
            'height' => 220,
            'path'   => 'assets/img/servicos-abertura/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 220,
            'height' => 220,
            'path'   => 'assets/img/servicos-abertura/'
        ]);
    }

}
