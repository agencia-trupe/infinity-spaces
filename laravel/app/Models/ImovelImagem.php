<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImovelImagem extends Model
{
    protected $table = 'imoveis_imagens';

    protected $guarded = ['id'];

    public function scopeImovel($query, $id)
    {
        return $query->where('imovel_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
