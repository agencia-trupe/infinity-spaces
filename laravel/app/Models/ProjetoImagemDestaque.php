<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjetoImagemDestaque extends Model
{
    protected $table = 'projetos_imagens_destaque';

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
