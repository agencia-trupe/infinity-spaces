<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;
use Carbon\Carbon;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Midia extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'midia';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

    public function getDataFormatadaAttribute()
    {
        $meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

        list($mes, $ano) = explode('/', $this->data);

        return $meses[(int) $mes - 1] . ' ' . $ano;
    }

    public function getDataOrdenavelAttribute()
    {
        return Carbon::createFromFormat('m/Y', $this->data)->format('Y-m');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => null,
            'height' => 300,
            'path'   => 'assets/img/midia/'
        ]);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\MidiaImagem', 'midia_id')->ordenados();
    }
}
