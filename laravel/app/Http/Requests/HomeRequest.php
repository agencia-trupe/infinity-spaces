<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada_titulo' => 'required',
            'chamada_texto' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'servicos_titulo' => 'required',
            'servicos_texto' => 'required',
            'projetos_titulo' => 'required',
            'projetos_texto' => 'required',
            'projetos_imagem' => 'image',
        ];
    }
}
