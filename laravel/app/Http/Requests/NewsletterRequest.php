<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'  => 'required',
            'email' => 'email|required|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'  => trans('frontend.footer.nome_req'),
            'email.required' => trans('frontend.footer.email_req'),
            'email.email'    => trans('frontend.footer.email_val'),
            'email.unique'   => trans('frontend.footer.email_unique'),
        ];
    }
}
