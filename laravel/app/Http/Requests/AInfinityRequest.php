<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AInfinityRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
            'imagem_6' => 'image',
            'titulo' => 'required',
            'texto' => 'required',
            'pessoas_chave_foto' => 'image',
            'pessoas_chave_texto' => 'required',
        ];
    }
}
