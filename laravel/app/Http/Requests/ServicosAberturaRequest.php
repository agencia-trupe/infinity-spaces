<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosAberturaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'texto' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
        ];
    }
}
