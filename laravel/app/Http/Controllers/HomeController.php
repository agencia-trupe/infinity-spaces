<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\NewsletterRequest;

use App\Models\Banner;
use App\Models\Home;
use App\Models\Newsletter;
use App\Models\Servico;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home')->with([
            'banners'  => Banner::ordenados()->get(),
            'home'     => Home::first(),
            'servicos' => Servico::ordenados()->get()
        ]);
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.footer.sucesso')
        ];

        return response()->json($response);
    }
}
