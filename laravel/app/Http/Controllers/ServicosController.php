<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ServicosAbertura;
use App\Models\Servico;

class ServicosController extends Controller
{
    public function index()
    {
        return view('frontend.servicos')->with([
            'abertura' => ServicosAbertura::first(),
            'servicos' => Servico::ordenados()->get()
        ]);
    }
}
