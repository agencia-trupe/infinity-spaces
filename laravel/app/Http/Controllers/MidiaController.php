<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Midia;

class MidiaController extends Controller
{
    public function index()
    {
        $publicacoes = Midia::ordenados()->get();

        return view('frontend.midia.index', compact('publicacoes'));
    }

    public function show(Midia $midia)
    {
        $anterior = Midia::select('slug')
                        ->where('data', '<', $midia->data_ordenavel)
                        ->orWhere(function($query) use ($midia) {
                            $query->where('data', '=', $midia->data_ordenavel)
                                  ->where('id', '<', $midia->id);
                        })
                        ->orderBy('data', 'DESC')
                        ->orderBy('id', 'DESC')
                        ->first();

        $proximo  = Midia::select('slug')
                        ->where('data', '>', $midia->data_ordenavel)
                        ->orWhere(function($query) use ($midia) {
                            $query->where('data', '=', $midia->data_ordenavel)
                                  ->where('id', '>', $midia->id);
                        })
                        ->orderBy('data', 'ASC')
                        ->orderBy('id', 'ASC')
                        ->first();

        return view('frontend.midia.show', compact('midia', 'anterior', 'proximo'));
    }
}
