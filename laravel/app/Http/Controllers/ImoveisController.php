<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Imovel;

class ImoveisController extends Controller
{
    public function index()
    {
        $imoveis = Imovel::ordenados()->get();

        return view('frontend.imoveis.index', compact('imoveis'));
    }

    public function show(Imovel $imovel)
    {
        return view('frontend.imoveis.show', compact('imovel'));
    }
}
