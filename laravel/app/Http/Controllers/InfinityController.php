<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AInfinity;

class InfinityController extends Controller
{
    public function index()
    {
        return view('frontend.infinity')->with([
            'infinity' => AInfinity::first()
        ]);
    }
}
