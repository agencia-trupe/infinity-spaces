<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosAberturaRequest;
use App\Http\Controllers\Controller;

use App\Models\ServicosAbertura;

class ServicosAberturaController extends Controller
{
    public function index()
    {
        $registro = ServicosAbertura::first();

        return view('painel.servicos.abertura.edit', compact('registro'));
    }

    public function update(ServicosAberturaRequest $request, ServicosAbertura $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = ServicosAbertura::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = ServicosAbertura::upload_imagem_2();

            $registro->update($input);

            return redirect()->route('painel.servicos.abertura.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
