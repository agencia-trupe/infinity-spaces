<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImovelRequest;
use App\Http\Controllers\Controller;

use App\Models\Imovel;
use App\Helpers\CropImage;

class ImoveisController extends Controller
{
    private $categorias;

    private $image_config = [
        'width'   => 365,
        'height'  => 245,
        'path'    => 'assets/img/imoveis/thumbs/'
    ];

    public function index(Request $request)
    {
        $imoveis = Imovel::ordenados()->get();

        return view('painel.imoveis.index', compact('imoveis'));
    }

    public function create()
    {
        return view('painel.imoveis.create');
    }

    public function store(ImovelRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Imovel::create($input);
            return redirect()->route('painel.imoveis.index')->with('success', 'Imóvel adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar imóvel: '.$e->getMessage()]);

        }
    }

    public function edit(Imovel $imovel)
    {
        return view('painel.imoveis.edit', compact('imovel'));
    }

    public function update(ImovelRequest $request, Imovel $imovel)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imovel->update($input);
            return redirect()->route('painel.imoveis.index')->with('success', 'Imóvel alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar imóvel: '.$e->getMessage()]);

        }
    }

    public function destroy(Imovel $projeto)
    {
        try {

            $projeto->delete();
            return redirect()->route('painel.imoveis.index')->with('success', 'Imóvel excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imóvel: '.$e->getMessage()]);

        }
    }
}
