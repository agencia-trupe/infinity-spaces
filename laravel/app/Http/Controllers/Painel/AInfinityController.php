<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AInfinityRequest;
use App\Http\Controllers\Controller;

use App\Models\AInfinity;

class AInfinityController extends Controller
{
    public function index()
    {
        $registro = AInfinity::first();

        return view('painel.a-infinity.edit', compact('registro'));
    }

    public function update(AInfinityRequest $request, AInfinity $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = AInfinity::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = AInfinity::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = AInfinity::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = AInfinity::upload_imagem_4();
            if (isset($input['imagem_5'])) $input['imagem_5'] = AInfinity::upload_imagem_5();
            if (isset($input['imagem_6'])) $input['imagem_6'] = AInfinity::upload_imagem_6();
            if (isset($input['pessoas_chave_foto'])) $input['pessoas_chave_foto'] = AInfinity::upload_pessoas_chave_foto();

            $registro->update($input);

            return redirect()->route('painel.a-infinity.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
