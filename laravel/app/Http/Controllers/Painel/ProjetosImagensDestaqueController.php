<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetoImagemDestaqueRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoImagemDestaque;
use App\Helpers\CropImage;

class ProjetosImagensDestaqueController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/projetos/imagens-destaque/thumbs/'
        ],
        [
            'width'   => 800,
            'height'  => 525,
            'bg'      => '#F3F3F3',
            'path'    => 'assets/img/projetos/imagens-destaque/thumbs-grande/'
        ],
        [
            'width'   => 1980,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/projetos/imagens-destaque/'
        ]
    ];

    public function index(Projeto $projeto)
    {
        $imagens = ProjetoImagemDestaque::projeto($projeto->id)->ordenados()->get();

        return view('painel.projetos.imagens-destaque.index', compact('imagens', 'projeto'));
    }

    public function show(Projeto $projeto, ProjetoImagemDestaque $imagem)
    {
        return $imagem;
    }

    public function create(Projeto $projeto)
    {
        return view('painel.projetos.imagens-destaque.create', compact('projeto'));
    }

    public function store(Projeto $projeto, ProjetoImagemDestaqueRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['projeto_id'] = $projeto->id;

            $imagem = ProjetoImagemDestaque::create($input);

            $view = view('painel.projetos.imagens-destaque.imagem', compact('projeto', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Projeto $projeto, ProjetoImagemDestaque $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.projetos.imagens-destaque.index', $projeto)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Projeto $projeto)
    {
        try {

            $projeto->imagensDestaque()->delete();
            return redirect()->route('painel.projetos.imagens-destaque.index', $projeto)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
