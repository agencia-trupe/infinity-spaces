<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MidiaImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Midia;
use App\Models\MidiaImagem;
use App\Helpers\CropImage;

class MidiaImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 360,
            'height'  => 180,
            'path'    => 'assets/img/midia/imagens/thumbs/'
        ],
        [
            'width'   => 940,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/midia/imagens/'
        ]
    ];

    public function index(Midia $midia)
    {
        $imagens = MidiaImagem::midia($midia->id)->ordenados()->get();

        return view('painel.midia.imagens.index', compact('imagens', 'midia'));
    }

    public function show(Midia $midia, MidiaImagem $imagem)
    {
        return $imagem;
    }

    public function create(Midia $midia)
    {
        return view('painel.midia.imagens.create', compact('midia'));
    }

    public function store(Midia $midia, MidiaImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = $midia->imagens()->create($input);

            $view = view('painel.midia.imagens.imagem', compact('midia', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Midia $midia, MidiaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.midia.imagens.index', $midia)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Midia $midia)
    {
        try {

            $midia->imagens()->delete();
            return redirect()->route('painel.midia.imagens.index', $midia)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
