<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImovelImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\Imovel;
use App\Models\ImovelImagem;
use App\Helpers\CropImage;

class ImoveisImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 230,
            'height'  => 175,
            'path'    => 'assets/img/imoveis/imagens/thumbs/'
        ],
        [
            'width'   => 730,
            'height'  => 410,
            'path'    => 'assets/img/imoveis/imagens/thumbs-destaque/'
        ],
        [
            'width'   => 1980,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/imoveis/imagens/'
        ]
    ];

    public function index(Imovel $imovel)
    {
        $imagens = $imovel->imagens;

        return view('painel.imoveis.imagens.index', compact('imagens', 'imovel'));
    }

    public function show(Imovel $imovel, ImovelImagem $imagem)
    {
        return $imagem;
    }

    public function create(Imovel $imovel)
    {
        return view('painel.imoveis.imagens.create', compact('imovel'));
    }

    public function store(Imovel $imovel, ImovelImagemRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['imovel_id'] = $imovel->id;

            $imagem = ImovelImagem::create($input);

            $view = view('painel.imoveis.imagens.imagem', compact('imovel', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Imovel $imovel, ImovelImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.imoveis.imagens.index', $imovel)
                ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Imovel $imovel)
    {
        try {

            $imovel->imagens()->delete();
            return redirect()->route('painel.imoveis.imagens.index', $imovel)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
