<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('a-infinity', 'InfinityController@index')->name('a-infinity');
    Route::get('servicos', 'ServicosController@index')->name('servicos');
    Route::get('projetos/{categoria_slug?}', 'ProjetosController@index')->name('projetos');
    Route::get('projetos/{categoria_slug}/{projeto_slug}', 'ProjetosController@show')->name('projetos.show');
    Route::get('midia', 'MidiaController@index')->name('midia');
    Route::get('midia/{midia_slug}', 'MidiaController@show')->name('midia.show');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('imoveis', 'ImoveisController@index')->name('imoveis');
    Route::get('imoveis/{imovel_slug}', 'ImoveisController@show')->name('imoveis.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en') Session::put('locale', $idioma);
        return redirect()->route('home');
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('imoveis', 'ImoveisController');
        Route::get('imoveis/{imoveis}/imagens/clear', [
            'as'   => 'painel.imoveis.imagens.clear',
            'uses' => 'ImoveisImagensController@clear'
        ]);
        Route::resource('imoveis.imagens', 'ImoveisImagensController');

        Route::resource('projetos/categorias', 'ProjetosCategoriasController');
        Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');
        Route::get('projetos/{projetos}/imagens-destaque/clear', [
            'as'   => 'painel.projetos.imagens-destaque.clear',
            'uses' => 'ProjetosImagensDestaqueController@clear'
        ]);
        Route::resource('projetos.imagens-destaque', 'ProjetosImagensDestaqueController');
        Route::resource('clientes', 'ClientesController');
		Route::resource('midia', 'MidiaController');
        Route::get('midia/{midia}/imagens/clear', [
            'as'   => 'painel.midia.imagens.clear',
            'uses' => 'MidiaImagensController@clear'
        ]);
        Route::resource('midia.imagens', 'MidiaImagensController');
		Route::resource('a-infinity', 'AInfinityController', ['only' => ['index', 'update']]);
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('servicos/abertura', 'ServicosAberturaController', ['only' => ['index', 'update']]);
		Route::resource('servicos', 'ServicosController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
