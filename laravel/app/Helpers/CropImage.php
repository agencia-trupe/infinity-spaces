<?php

namespace App\Helpers;

use Request, Image;

class CropImage
{

    public static function make($input, $object)
    {
        if (!Request::hasFile($input) || !$object) return false;

        $image = Request::file($input);

        $name  = str_slug(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$image->getClientOriginalExtension();

        if (!is_array(array_values($object)[0])) $object = array($object);

        foreach($object as $config) {
            $width  = $config['width'];
            $height = $config['height'];
            $path   = $config['path'].$name;
            $upsize = (array_key_exists('upsize', $config) ? $config['upsize'] : false);
            $bgcolor = (array_key_exists('bg', $config) ? $config['bg'] : false);
            $cliente = (array_key_exists('cliente', $config) ? $config['cliente'] : false);
            $marca = (array_key_exists('marca', $config) ? $config['marca'] : false);

            if (!file_exists(public_path($config['path']))) {
                mkdir(public_path($config['path']), 0777, true);
            }

            $imgobj = Image::make($image->getRealPath());

            if ($width > 0 && $height > 0) {
                $ratio  = $imgobj->width() / $imgobj->height();
            }

            if ($width == null && $height == null) {
            } elseif ($width == null || $height == null) {
                $imgobj->resize($width, $height, function($constraint) use ($upsize) {
                    $constraint->aspectRatio();
                    if ($upsize) { $constraint->upsize(); }
                });
            } elseif ($cliente) {
                $canvas = Image::canvas($width, $height, '#fff');
                $imagem = Image::make($imgobj)->resize($width, $height, function($constraint)
                {
                    $constraint->aspectRatio();
                });
                $imgobj = $canvas->insert($imagem, 'center');
            } elseif ($bgcolor && $ratio <= 1) {
                $canvas = Image::canvas($width, $height, $bgcolor);
                $imagem = Image::make($imgobj)->resize($width, $height, function($constraint)
                {
                    $constraint->aspectRatio();
                });
                $imgobj = $canvas->insert($imagem, 'center');
            } else {
                $imgobj->fit($width, $height, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                });
            }

            if ($marca) {
                $imgobj->insert($config['marca'], 'center');
            }

            $imgobj->save($path, 100)->destroy();
        }

        return $name;
    }

}
