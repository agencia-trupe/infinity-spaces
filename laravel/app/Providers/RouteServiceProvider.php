<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('imoveis', 'App\Models\Imovel');
        $router->model('categorias', 'App\Models\ProjetoCategoria');
        $router->model('projetos', 'App\Models\Projeto');
        $router->model('imagens_destaque', 'App\Models\ProjetoImagemDestaque');
        $router->model('clientes', 'App\Models\Cliente');
		$router->model('midia', 'App\Models\Midia');
		$router->model('a-infinity', 'App\Models\AInfinity');
		$router->model('home', 'App\Models\Home');
		$router->model('banners', 'App\Models\Banner');
		$router->model('abertura', 'App\Models\ServicosAbertura');
		$router->model('servicos', 'App\Models\Servico');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('imagens', function($id, $route, $model = null) {
            if ($route->hasParameter('midia')) {
                $model = \App\Models\MidiaImagem::find($id);
            } elseif ($route->hasParameter('projetos')) {
                $model = \App\Models\ProjetoImagem::find($id);
            } elseif ($route->hasParameter('imoveis')) {
                $model = \App\Models\ImovelImagem::find($id);
            }

            return $model ?: abort('404');
        });

        $router->bind('categoria_slug', function($value) {
            return \App\Models\ProjetoCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('projeto_slug', function($value) {
            return \App\Models\Projeto::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('imovel_slug', function($value) {
            return \App\Models\Imovel::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('midia_slug', function($value) {
            return \App\Models\Midia::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
