<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosAberturaTable extends Migration
{
    public function up()
    {
        Schema::create('servicos_abertura', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('servicos_abertura');
    }
}
