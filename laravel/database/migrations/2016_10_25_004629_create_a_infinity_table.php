<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAInfinityTable extends Migration
{
    public function up()
    {
        Schema::create('a_infinity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->string('imagem_6');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('pessoas_chave_foto');
            $table->text('pessoas_chave_texto');
            $table->text('pessoas_chave_texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('a_infinity');
    }
}
