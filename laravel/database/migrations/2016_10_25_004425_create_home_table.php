<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('chamada_titulo');
            $table->text('chamada_titulo_en');
            $table->text('chamada_texto');
            $table->text('chamada_texto_en');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->text('servicos_titulo');
            $table->text('servicos_titulo_en');
            $table->text('servicos_texto');
            $table->text('servicos_texto_en');
            $table->text('projetos_titulo');
            $table->text('projetos_titulo_en');
            $table->text('projetos_texto');
            $table->text('projetos_texto_en');
            $table->string('projetos_imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
