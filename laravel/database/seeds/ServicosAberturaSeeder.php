<?php

use Illuminate\Database\Seeder;

class ServicosAberturaSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos_abertura')->insert([
            'titulo' => '',
            'texto' => '',
            'imagem_1' => '',
            'imagem_2' => '',
        ]);
    }
}
