<?php

use Illuminate\Database\Seeder;

class AInfinitySeeder extends Seeder
{
    public function run()
    {
        DB::table('a_infinity')->insert([
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
            'imagem_6' => '',
            'titulo' => '',
            'texto' => '',
            'pessoas_chave_foto' => '',
            'pessoas_chave_texto' => '',
        ]);
    }
}
