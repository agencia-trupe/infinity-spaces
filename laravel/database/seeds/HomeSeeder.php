<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'chamada_titulo' => '',
            'chamada_texto' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'servicos_titulo' => '',
            'servicos_texto' => '',
            'projetos_titulo' => '',
            'projetos_texto' => '',
            'projetos_imagem' => '',
        ]);
    }
}
