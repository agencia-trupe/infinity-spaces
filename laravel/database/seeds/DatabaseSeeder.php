<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(AInfinitySeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(ServicosAberturaSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
